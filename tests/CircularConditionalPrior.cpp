#include <iostream>
#include <fstream>
#include <ctime>
#include "DNest4/code/RNG.h"
#include "../src/Data.h"
#include "../src/CircularConditionalPrior.h"

int main()
{
    // Load a dataset
    Lensing3::Data::get_instance().load("../src/data/0047_metadata.txt",
                                        "../src/data/0047_image.txt",
                                        "../src/data/0047_sigma.txt",
                                        "../src/data/0047_psf.txt");

    // An RNG
    DNest4::RNG rng(time(0));

    // Something to test
    Lensing3::CircularConditionalPrior ccp;

    for(int i=0; i<1000; ++i)
    {
        ccp.from_prior(rng);

        std::vector<double> u(4);
        for(double& uu: u)
            uu = rng.rand();

        std::cout<<"These should be the same:\n";
        for(double& uu: u)
            std::cout<<uu<<' ';
        std::cout<<std::endl;

        ccp.from_uniform(u);
        ccp.to_uniform(u);
        for(double& uu: u)
            std::cout<<uu<<' ';
        std::cout<<'\n'<<std::endl;
    }

    // Now use the Metropolis algorithm
    ccp = Lensing3::CircularConditionalPrior(1.2, -3.3, 1.5, 0.7, 1.9, 0.4, 0.3);
    std::vector<double> x{1.0, 1.0, 1.0, 1.0};
    double logp = ccp.log_pdf(x);
    std::fstream fout("output.txt", std::ios::out);

    for(int i=0; i<10000000; ++i)
    {
        auto proposal = x;
        int which = rng.rand_int(proposal.size());

        proposal[which] += exp(5*rng.randn())*rng.randn();
        double logp2 = ccp.log_pdf(proposal);

        if(rng.rand() <= exp(logp2 - logp))
        {
            x = proposal;
            logp = logp2;
        }

        if((i+1) % 10 == 0)
        {
            for(double xx: x)
                fout<<xx<<' ';
            fout<<std::endl;
            std::cout<<(i+1)<<' '<<logp<<std::endl;
        }
    }
    fout.close();

    return 0;
}

