#include <iostream>
#include "../src/Data.h"

int main()
{
    Lensing3::Data data;
    data.load("../src/data/0047_metadata.txt",
              "../src/data/0047_image.txt",
              "../src/data/0047_sigma.txt",
              "../src/data/0047_psf.txt");

    return 0;
}

