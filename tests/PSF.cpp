#include <iostream>
#include <ctime>
#include "DNest4/code/RNG.h"
#include "../src/PSF.cpp"

int main()
{
    // An RNG
    DNest4::RNG rng(time(0));

    // Construct and print a PSF
    Lensing3::PSF psf(3, 5);
    psf.load("test_psf.txt");

    // Construct a test image to blur
    arma::mat image(10, 7, arma::fill::zeros);
    image(5, 5) = 1.0;

    psf.calculate_fft(image.n_rows, image.n_cols);

    psf.blur(image);
    for(size_t i=0; i<image.n_rows; ++i)
    {
        for(size_t j=0; j<image.n_cols; ++j)
            std::cout<<image(i, j)<<' ';
        std::cout<<std::endl;
    }

    return 0;
}

