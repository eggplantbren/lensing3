#include "SPEMD.h"
#include "Data.h"
#include "DNest4/code/Utils.h"
#include <cmath>
#include <exception>

namespace Lensing3
{

extern "C"
{
    void fastelldefl_(double* x, double* y, double* b, double* gam, double* q,
                        double* rcsq, double alpha[]);
}

SPEMD::SPEMD()
{

}

std::tuple<double, double> SPEMD::alpha(double x, double y)
{
    double ax = 0.0;
    double ay = 0.0;

    // Rotate and center
    double xx =  (x - xc)*cos_theta + (y - yc)*sin_theta;
    double yy = -(x - xc)*sin_theta + (y - yc)*cos_theta;

    // Evaluate fastelldefl
    double aa[2];
    double rcsq = rc*rc;
    double coeff = 0.5*pow(bb, 2.0*slope)*(2.0 - 2.0*slope);

    fastelldefl_(&xx, &yy, &coeff, &slope, &qq, &rcsq, aa);
    double alphax = aa[0];
    double alphay = aa[1];

    // Rotate back
    ax = alphax*cos_theta - alphay*sin_theta;
    ay = alphax*sin_theta + alphay*cos_theta;

    // Go into shear coordinate system
    xx =  x*cos_theta_shear + y*sin_theta_shear;
    yy = -x*sin_theta_shear + y*cos_theta_shear;

    // Calculate external shear
    alphax = -shear*xx;
    alphay = shear*yy;

    // Add external shear
    ax += alphax*cos_theta_shear - alphay*sin_theta_shear;
    ay += alphax*sin_theta_shear + alphay*cos_theta_shear;

    return std::tuple<double, double>(ax, ay);
}

void SPEMD::from_prior(DNest4::RNG& rng)
{
    // Copy of the metadata
    const auto& md = Data::get_instance().get_metadata();

    // p(b) \propto b, b \in [0, scale]
    while(true)
    {
        b = rng.rand();
        if(rng.rand() <= (b/md.scale))
            break;
    }

    // q ~ U(0.05, 1)
    q = 0.05 + 0.95*rng.rand();

    // Stuff derived from b and q
    qq = q;
    if(qq == 1.)
        qq = 0.99999;
    bb = b/sqrt(qq);    // Semi-major axis

    // rc ~ logUniform(1E-3*scale, scale)
    rc = exp(log(1E-3) + log(1E3)*rng.rand())*md.scale;

    // slope ~ U(0.1, 0.9)
    slope = 0.1 + 0.8*rng.rand();

    // xc, yc ~ Cauchy(image center, 0.1*image width)
    // (xc, yc) \in image domain
    while(true)
    {
        xc = md.x_min + (md.x_max - md.x_min)*rng.rand();
        yc = md.y_min + (md.y_max - md.y_min)*rng.rand();

        double rx = (xc - md.x_mid)/(0.1*md.x_range);
        double ry = (yc - md.y_mid)/(0.1*md.y_range);

        double px = 1.0 / (1.0 + rx*rx);
        double py = 1.0 / (1.0 + ry*ry);

        if(rng.rand() <= px*py)
            break;
    }

    theta = M_PI*rng.rand();
    cos_theta = cos(theta); sin_theta = sin(theta);

    // Half-cauchy prior
    shear = 0.05*tan(M_PI*(0.5*rng.rand()));
    theta_shear = M_PI*rng.rand();
    cos_theta_shear = cos(theta_shear); sin_theta_shear = sin(theta_shear);
}

double SPEMD::perturb(DNest4::RNG& rng)
{
    // Copy of the metadata
    const auto& md = Data::get_instance().get_metadata();

    double logH = 0.0;

    int which = rng.rand_int(9);

    if(which == 0)
    {
        logH -= log(b/md.scale);
        b += md.scale*rng.randh();
        DNest4::wrap(b, 0.0, md.scale);
        logH += log(b/md.scale);

        // Stuff derived from b and q
        qq = q;
        if(qq == 1.0)
            qq = 0.99999;
        bb = b/sqrt(qq); // Semi-major axis
    }
    else if(which == 1)
    {
        q += 0.95*rng.randh();
        DNest4::wrap(q, 0.05, 1.0);

        // Stuff derived from b and q
        qq = q;
        if(qq == 1.)
            qq = 0.99999;
        bb = b/sqrt(qq); // Semi-major axis
    }
    else if(which == 2)
    {
        rc = log(rc/md.scale);
        rc += log(1E3)*rng.randh();
        DNest4::wrap(rc, log(1E-3), 0.0);
        rc = md.scale*exp(rc);
    }
    else if(which == 3)
    {
        slope += 0.8*rng.randh();
        DNest4::wrap(slope, 0.1, 0.9);
    }
    else if(which == 4)
    {
        double rx;

        rx = (xc - md.x_mid)/(0.1*md.x_range);
        logH -= -log(1.0 + rx*rx);

        xc += md.x_range * rng.randh();

        DNest4::wrap(xc, md.x_min, md.x_max);

        rx = (xc - md.x_mid)/(0.1*md.x_range);
        logH += -log(1.0 + rx*rx);
    }
    else if(which == 5)
    {
        double ry;

        ry = (yc - md.y_mid)/(0.1*md.y_range);
        logH -= -log(1.0 + ry*ry);

        yc += md.y_range * rng.randh();

        DNest4::wrap(yc, md.y_min, md.y_max);

        ry = (yc - md.y_mid)/(0.1*md.y_range);
        logH += -log(1.0 + ry*ry);
    }
    else if(which == 6)
    {
        theta += M_PI*rng.randh();
        DNest4::wrap(theta, 0.0, M_PI);
        cos_theta = cos(theta); sin_theta = sin(theta);
    }
    else if(which == 7)
    {
        shear = atan(shear/0.05)/M_PI/0.5;
        shear += rng.randh();
        DNest4::wrap(shear, 0.0, 1.0);
        shear = 0.05*tan(M_PI*(0.5*shear));
    }
    else
    {
        theta_shear += M_PI*rng.randh();
        DNest4::wrap(theta_shear, 0.0, M_PI);
        cos_theta_shear = cos(theta_shear); sin_theta_shear = sin(theta_shear);
    }

    return logH;
}

void SPEMD::print(std::ostream& out) const
{
    out<<b<<' '<<q<<' '<<rc<<' '<<slope<<' '<<xc<<' '<<yc<<' '<<theta<<' ';
    out<<shear<<' '<<theta_shear<<' ';
}

void SPEMD::read(std::istream& in)
{
    in>>b>>q>>rc>>slope>>xc>>yc>>theta>>shear>>theta_shear;
}

} // namespace Lensing3

