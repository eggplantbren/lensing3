#ifndef Lensing3_SPEMD
#define Lensing3_SPEMD

#include <istream>
#include <ostream>
#include <tuple>
#include "DNest4/code/RNG.h"

namespace Lensing3
{

/*
* A class for a single SPEMD.
*/
class SPEMD
{
    private:
        // Strength, axis ratio, core radius, slope
        double b, q, rc, slope;

        // Derived parameters based on b and q
        double bb, qq;

        // Position
        double xc, yc;

        // Orientation angle
        double theta, cos_theta, sin_theta;

        // External shear
        double shear;
        double theta_shear, cos_theta_shear, sin_theta_shear;

    public:
        // Do-nothing constructor
        SPEMD();

        // Deflection angle formula
        std::tuple<double, double> alpha(double x, double y);

        // Generate parameter from prior
        void from_prior(DNest4::RNG& rng);

        // Metropolis-Hastings proposal
        double perturb(DNest4::RNG& rng);

        // Print to output stream
        void print(std::ostream& out) const;

        // Read from an input stream
        void read(std::istream& in);
};

} // namespace Lensing3

#endif

