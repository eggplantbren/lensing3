#ifndef Lensing3_MyModel
#define Lensing3_MyModel

#include "BlobbySource.h"
#include "SPEMD.h"
#include "DNest4/code/RNG.h"
#include <ostream>
#include <armadillo>

namespace Lensing3
{

class MyModel
{
    private:
        // The source and lens
        BlobbySource source;
        SPEMD lens;

        // Source plane positions of rays, and function to compute them
        arma::mat xs_rays, ys_rays;
        void shoot_rays();

        // Model image, and function to compute it
        arma::mat image;
        void compute_image();

    public:
        // Constructor only gives size of params
        MyModel();

        // Generate the point from the prior
        void from_prior(DNest4::RNG& rng);

        // Metropolis-Hastings proposals
        double perturb(DNest4::RNG& rng);

        // Likelihood function
        double log_likelihood() const;

        // Print to stream
        void print(std::ostream& out) const;

        // Return string with column information
        std::string description() const;
};

} // namespace Lensing3

#endif

