#include "CircularConditionalPrior.h"
#include "DNest4/code/DNest4.h"
#include "Data.h"
#include <cmath>

namespace Lensing3
{

DNest4::Cauchy CircularConditionalPrior::cauchy(0.0, 5.0);

CircularConditionalPrior::CircularConditionalPrior()
{

}

CircularConditionalPrior::CircularConditionalPrior
            (double xc, double yc,
             double location_log_r, double scale_log_r,
             double mu_mass, double location_log_width,
             double scale_log_width)
:xc(xc),yc(yc)
,location_log_r(location_log_r), scale_log_r(scale_log_r)
,mu_mass(mu_mass), location_log_width(location_log_width)
,scale_log_width(scale_log_width)
{

}

void CircularConditionalPrior::from_prior(DNest4::RNG& rng)
{
    // Reference to the metadata
    const auto& md = Data::get_instance().get_metadata();

    // Uniform prior for central position
    xc = md.x_min + md.x_range*rng.rand();
    yc = md.y_min + md.y_range*rng.rand();

    // Hyperparameters for distance of blobs from central position
    location_log_r = log(1E-3*md.scale) + log(1E3)*rng.rand();
    scale_log_r = 2.0*rng.rand();

    // Very wide prior for mu
    do
    {
        mu_mass = cauchy.generate(rng);
    }while(std::abs(mu_mass) >= 50.0);
    mu_mass = exp(mu_mass);

    // Hyperparameters for widths
    location_log_width = log(1E-3*md.scale) + log(1E3)*rng.rand();
    scale_log_width = 2.0*rng.rand();
}

double CircularConditionalPrior::perturb_hyperparameters(DNest4::RNG& rng)
{
    const auto& md = Data::get_instance().get_metadata();

    double logH = 0.0;

    int which = rng.rand_int(7);
    if(which == 0)
    {
        xc += md.x_range*rng.randh();
        DNest4::wrap(xc, md.x_min, md.x_max);
    }
    else if(which == 1)
    {
        yc += md.y_range*rng.randh();
        DNest4::wrap(yc, md.y_min, md.y_max);
    }
    else if(which == 2)
    {
        location_log_r += log(1E3)*rng.randh();
        DNest4::wrap(location_log_r, log(1E-3*md.scale), log(md.scale));
    }
    else if(which == 3)
    {
        scale_log_r += 2.0*rng.randh();
        DNest4::wrap(scale_log_r, 0.0, 2.0);
    }
    else if(which == 4)
    {
        mu_mass = log(mu_mass);
        logH += cauchy.perturb(mu_mass, rng);
        if(std::abs(mu_mass) >= 50.0)
            return -1E300;
        mu_mass = exp(mu_mass);
    }
    else if(which == 5)
    {
        location_log_width += log(1E3)*rng.randh();
        DNest4::wrap(location_log_width, log(1E-3*md.scale), log(md.scale));
    }
    else
    {
        scale_log_width += 2.0*rng.randh();
        DNest4::wrap(scale_log_width, 0.0, 2.0);
    }

    return logH;
}

// vec = {x, y, mass, width}

// dx dy = r dr dphi
// r = exp(u)
// dr = exp(u) du = r du
// dx dy = r^2 dr dphi

double CircularConditionalPrior::log_pdf(const std::vector<double>& vec) const
{
    double logp = 0.0;

    // Any hard limits
    if(vec[2] < 0.0 || vec[3] < 0.0)
        return -1E300;

    // Laplace distribution
    DNest4::Laplace laplace(location_log_r, scale_log_r);

    // r and phi
    double r   = sqrt(pow(vec[0] - xc, 2) + pow(vec[1] - yc, 2));
    double phi = atan2(vec[1] - yc, vec[0] - xc);
    if(phi < 0.0)
        phi += 2.0*M_PI;
    double logr = log(r);

    logp += -2*logr + laplace.log_pdf(logr) - log(2.0*M_PI);

    // Mass
    logp += -log(mu_mass) - vec[2]/mu_mass;

    // Width
    double log_width = log(vec[3]);
    laplace = DNest4::Laplace(location_log_width, scale_log_width);
    logp += laplace.log_pdf(log_width) - log_width;

    return logp;
}

void CircularConditionalPrior::from_uniform(std::vector<double>& vec) const
{
    // Laplace distribution
    DNest4::Laplace laplace(location_log_r, scale_log_r);
    double r   = exp(laplace.cdf_inverse(vec[0]));
    double phi = 2.0*M_PI*vec[1];

    // x and y
    vec[0] = xc + r*cos(phi);
    vec[1] = yc + r*sin(phi);

    // Mass
    vec[2] = -mu_mass * log(1.0 - vec[2]);

    // Width
    laplace = DNest4::Laplace(location_log_width, scale_log_width);
    vec[3] = exp(laplace.cdf_inverse(vec[3]));
}

void CircularConditionalPrior::to_uniform(std::vector<double>& vec) const
{
    // Laplace distribution
    DNest4::Laplace laplace(location_log_r, scale_log_r);

    // r and phi
    double r   = sqrt(pow(vec[0] - xc, 2) + pow(vec[1] - yc, 2));
    double phi = atan2(vec[1] - yc, vec[0] - xc);
    if(phi < 0.0)
        phi += 2.0*M_PI;

    vec[0] = laplace.cdf(log(r));
    vec[1] = phi / (2.0 * M_PI);

    // Mass
    vec[2] = 1.0 - exp(-vec[2]/mu_mass);

    // Width
    laplace = DNest4::Laplace(location_log_width, scale_log_width);
    vec[3] = laplace.cdf(log(vec[3]));
}

void CircularConditionalPrior::print(std::ostream& out) const
{
    out<<xc<<' '<<yc<<' ';
    out<<location_log_r<<' '<<scale_log_r<<' ';
    out<<mu_mass<<' ';
    out<<location_log_width<<' '<<scale_log_width<<' ' ;
}

void CircularConditionalPrior::read(std::istream& in)
{
    in >> xc >> yc;
    in >> location_log_r >> scale_log_r;
    in >> mu_mass;
    in >> location_log_width >> scale_log_width;
}

} // namespace Lensing3

