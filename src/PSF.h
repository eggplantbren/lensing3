#ifndef Lensing3_PSF
#define Lensing3_PSF

#include <stdlib.h>
#include <armadillo>
#include <ostream>

namespace Lensing3
{

class PSF
{
	private:
        // PSF size (PSFs are all square)
		size_t ni, nj;

        // PSF pixel values
		arma::mat pixels;

		// FFT of the PSF
		arma::cx_mat fft_of_psf;
		bool fft_ready;

	public:
        // Constructor
		PSF(size_t ni, size_t nj);

		// Uses fft method
		void blur(arma::mat& img) const;
		void normalise();

        // Put the PSF on the same grid as an image and calculate its FFT
		void calculate_fft(size_t ni_image, size_t nj_image,
                                                double psf_power=1.0);
		void load(const char* filename);

		// Getter for pixels
		const arma::mat get_pixels() const
		{ return pixels; }

        // Print pixels
        friend std::ostream& operator << (std::ostream& out, const PSF& psf);

};

// Print pixels
std::ostream& operator << (std::ostream& out, const PSF& psf);

} // namespace Lensing3

#endif

