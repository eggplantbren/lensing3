#include "Data.h"
#include <fstream>

namespace Lensing3
{

Data Data::instance;

Data::Data()
:psf(1, 1)
{

}

void Data::load(const char* metadata_file,
                const char* image_file,
                const char* sigma_file,
                const char* psf_file)
{
    /*
    * First, read in the metadata
    */
    std::fstream fin(metadata_file, std::ios::in);
    if(!fin)
    {
        std::cerr << "# ERROR: couldn't open file " << metadata_file;
        std::cerr << "." << std::endl;
    }
    fin >> metadata.ni >> metadata.nj;
    fin >> metadata.x_min >> metadata.x_max;
    fin >> metadata.y_min >> metadata.y_max;
    fin >> metadata.ni_psf >> metadata.nj_psf;
    fin >> metadata.resolution;
    fin.close();

    // Make sure maximum > minimum
    if(metadata.x_max <= metadata.x_min ||
       metadata.y_max <= metadata.y_min)
    {
        std::cerr<<"# ERROR: strange input in "<<metadata_file;
        std::cerr<<"."<<std::endl;
    }

    // Compute derived stuff (pixel widths etc)
    metadata.dx = (metadata.x_max - metadata.x_min)/metadata.nj;
    metadata.dy = (metadata.y_max - metadata.y_min)/metadata.ni;

    // Spacing between rays
    metadata.L = metadata.resolution * sqrt(metadata.dx * metadata.dy);

    // Image range and midpoint
    metadata.x_range = metadata.x_max - metadata.x_min;
    metadata.y_range = metadata.y_max - metadata.y_min;
    metadata.x_mid = 0.5*(metadata.x_min + metadata.x_max);
    metadata.y_mid = 0.5*(metadata.y_min + metadata.y_max);

    // Scale length
    metadata.scale = sqrt(metadata.x_range * metadata.y_range);

    // Check that pixels are square
    if(std::abs(log(metadata.dx/metadata.dy)) >= 1E-6)
        std::cerr<<"# ERROR: pixels aren't square."<<std::endl;

    // There is now enough info loaded to compute the ray grid
    compute_ray_grid();

    // Load the image
    fin.open(image_file, std::ios::in);
    if(!fin)
    {
        std::cerr<<"# ERROR: couldn't open file "<<image_file;
        std::cerr<<"."<<std::endl;
    }
    image = arma::mat(metadata.ni, metadata.nj);

    // Images are stored in row major order on the disk...
    for(size_t i=0; i<metadata.ni; ++i)
        for(size_t j=0; j<metadata.nj; ++j)
            fin>>image(i, j);
    fin.close();

    // Load the sigma map
    fin.open(sigma_file, std::ios::in);
    if(!fin)
    {
        std::cerr<<"# ERROR: couldn't open file "<<sigma_file;
        std::cerr<<"."<<std::endl;
    }
    sigma = arma::mat(metadata.ni, metadata.nj);

    // Images are stored in row major order on the disk...
    for(size_t i=0; i<metadata.ni; ++i)
        for(size_t j=0; j<metadata.nj; ++j)
            fin>>sigma(i, j);
    fin.close();

    // Load the psf
    psf = PSF(metadata.ni_psf, metadata.nj_psf);
    psf.load(psf_file);
    psf.calculate_fft(metadata.ni, metadata.nj);

    // Now save the filenames used to run_data.txt
    std::fstream fout("run_data.txt", std::ios::out);
    fout << metadata_file << std::endl;
    fout << image_file    << std::endl;
    fout << sigma_file    << std::endl;
    fout << psf_file      << std::endl;
    fout.close();

    // Print messages
    std::cout<<"# Image size = "<<metadata.ni<<" x "<<metadata.nj<<" pixels.\n";
    std::cout<<"# (x_min, x_max, y_min, y_max) = (";
    std::cout<<metadata.x_min<<", "<<metadata.x_max<<", ";
    std::cout<<metadata.y_min<<", "<<metadata.y_max<<")"<<std::endl;
    std::cout<<std::endl;
}

void Data::compute_ray_grid()
{
    // Make arrays of the correct size
    x_rays = arma::mat(metadata.ni * metadata.resolution,
                       metadata.nj * metadata.resolution);
    y_rays = arma::mat(metadata.ni * metadata.resolution,
                       metadata.nj * metadata.resolution);

    // Compute the rays
    for(size_t j=0; j<x_rays.n_cols; ++j)
    {
        for(size_t i=0; i<x_rays.n_rows; ++i)
        {
            x_rays(i, j) = metadata.x_min + (j + 0.5)*metadata.L;
            y_rays(i, j) = metadata.y_max - (i + 0.5)*metadata.L;
        }
    }
}

} // namespace Lensing3

