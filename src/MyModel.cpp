#include "MyModel.h"
#include "Data.h"

#include <string>
#include <sstream>

namespace Lensing3
{

MyModel::MyModel()
:xs_rays(Data::get_instance().get_x_rays())
,ys_rays(Data::get_instance().get_y_rays())
,image(Data::get_instance().get_image())
{

}

void MyModel::from_prior(DNest4::RNG& rng)
{
    source.from_prior(rng);
    lens.from_prior(rng);

    shoot_rays();
    compute_image();
}

void MyModel::shoot_rays()
{
    const auto& x_rays = Data::get_instance().get_x_rays();
    const auto& y_rays = Data::get_instance().get_y_rays();

    for(size_t j=0; j<xs_rays.n_cols; ++j)
    {
        for(size_t i=0; i<xs_rays.n_rows; ++i)
        {
            std::tie(xs_rays(i, j), ys_rays(i, j)) =
                        lens.alpha(x_rays(i, j), y_rays(i, j));
        }
    }
}

void MyModel::compute_image()
{
    // Grab some stuff from the data
    const auto& data = Data::get_instance();

    PSF psf = data.get_psf();
    double x_min = data.get_metadata().x_min;
    double y_max = data.get_metadata().y_max;
    double dx = data.get_metadata().dx;
    double dy = data.get_metadata().dy;
    double dx_recip = 1.0/dx;
    double dy_recip = 1.0/dy;

    double resolution = data.get_metadata().resolution;
    double ray_weight = 1.0/pow(resolution, 2);

    // Zero the image
    for(size_t j=0; j<image.n_cols; ++j)
        for(size_t i=0; i<image.n_rows; ++i)
            image(i, j) = 0.0;

    // Loop over the rays
    int ii, jj;
    for(size_t j=0; j<xs_rays.n_cols; ++j)
    {
        for(size_t i=0; i<xs_rays.n_rows; ++i)
        {
            // Image pixel
            ii = (int)floor( (y_max - ys_rays(i, j)) * dy_recip );
            jj = (int)floor( (xs_rays(i, j) - x_min) * dx_recip );

            if(ii >= 0 && ii < (int)image.n_rows &&
               jj >= 0 && jj < (int)image.n_cols)
                image(ii, jj) += ray_weight * source.evaluate(xs_rays(i, j),
                                                              ys_rays(i, j));
        }
    }

    // Blur by the psf
    psf.blur(image);
}

double MyModel::perturb(DNest4::RNG& rng)
{
    double logH = 0.0;

    int which = rng.rand_int(2);

    if(which == 0)
        logH += source.perturb(rng);
    else
    {
        logH += lens.perturb(rng);
        shoot_rays();
    }

    compute_image();

    return logH;
}

double MyModel::log_likelihood() const
{
    // Grab the data
    const auto& data = Data::get_instance();
    const auto& img = data.get_image();
    const auto& sig = data.get_sigma();

    double logL = 0.0;

    double lp = log(2.0*M_PI);

    // Loop over the pixels
    for(size_t j=0; j<image.n_cols; ++j)
        for(size_t i=0; i<image.n_rows; ++i)
            logL += -log(sig(i, j)) - 0.5*lp
                        - 0.5*pow((img(i, j) - image(i, j))/sig(i, j), 2);

    return logL;
}

void MyModel::print(std::ostream& out) const
{
    //source.print(out);
    lens.print(out);
}

std::string MyModel::description() const
{
    std::stringstream s;
    s<<"b, q, rc, slope, xc, yc, theta, shear, theta_shear, ";
    return s.str();
}

} // namespace Lensing3

