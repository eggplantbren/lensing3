#ifndef Lensing3_BlobbySource
#define Lensing3_BlobbySource

#include "CircularConditionalPrior.h"
#include "DNest4/code/RJObject/RJObject.h"

namespace Lensing3
{

class BlobbySource
{
    private:
        DNest4::RJObject<CircularConditionalPrior> blobs;

    public:
        // Pass in image scale and flux scale
        BlobbySource();

        // Required methods
        double evaluate(double x, double y) const;
        void from_prior(DNest4::RNG& rng);
        double perturb(DNest4::RNG& rng);
        void print(std::ostream& out) const;
        void read(std::istream& in);
};

} // namespace Lensing3

#endif

