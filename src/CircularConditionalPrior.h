#ifndef Lensing3_CircularConditionalPrior
#define Lensing3_CircularConditionalPrior

#include "DNest4/code/DNest4.h"
#include <istream>

namespace Lensing3
{

/*
* Describes a conditional prior for the positions of things
* that is centered around a typical location, with a typical width.
* f(x,y) dx dy ~ f(r) dx dy
* Also for their masses.
*/

class CircularConditionalPrior:public DNest4::ConditionalPrior
{
    private:
        // A cauchy distribution
        static DNest4::Cauchy cauchy;

        // Central position
        double xc, yc;

        // Location and scale parameters for r
        double location_log_r, scale_log_r;

        // Scale parameter for mass
        double mu_mass;

        // Location and scale parameters for log(width)
        double location_log_width, scale_log_width;

        double perturb_hyperparameters(DNest4::RNG& rng);

    public:
        CircularConditionalPrior();
        CircularConditionalPrior(double xc, double yc,
                                 double location_log_r, double scale_log_r,
                                 double mu_mass, double location_log_width,
                                 double scale_log_width);

        void from_prior(DNest4::RNG& rng);

        double log_pdf(const std::vector<double>& vec) const;
        void from_uniform(std::vector<double>& vec) const;
        void to_uniform(std::vector<double>& vec) const;

        void print(std::ostream& out) const;
        void read(std::istream& in);
};

} // namespace Lensing3

#endif

