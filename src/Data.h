#ifndef Lensing3_Data
#define Lensing3_Data

#include "PSF.h"

namespace Lensing3
{

// Put the metadata in a struct
typedef struct
{
    // Stuff to be directly loaded from the metadata file
    size_t ni, nj;
    double x_min, x_max, y_min, y_max;
    size_t ni_psf, nj_psf;
    size_t resolution;

    // Derived stuff
    double dx, dy, L;
    double x_range, y_range, x_mid, y_mid;
    double scale;
} Metadata;

class Data
{
    private:
        // The metadata
        Metadata metadata;

        // The ray grid
        arma::mat x_rays, y_rays;

        // The pixels
        arma::mat image;

        // Sigma map
        arma::mat sigma;

        // The PSF
        PSF psf;

        void compute_ray_grid();

    public:
        // A do-nothing constructor
        Data();

        // Load data from a file.
        void load(const char* metadata_file,
                  const char* image_file,
                  const char* sigma_file,
                  const char* psf_file);

        // Getters
        const arma::mat& get_image() const { return image; }
        const arma::mat& get_sigma() const { return sigma; }
        const Metadata& get_metadata() const { return metadata; }
        const arma::mat& get_x_rays() const { return x_rays; }
        const arma::mat& get_y_rays() const { return y_rays; }
        const PSF& get_psf() const { return psf; }

    // Singleton instance
    private:
        static Data instance;

    public:
        static Data& get_instance() { return instance; }
};

} // namespace Lensing3

#endif

