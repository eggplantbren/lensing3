#include "BlobbySource.h"
#include "DNest4/code/RNG.h"

namespace Lensing3
{

BlobbySource::BlobbySource()
:blobs(4, 100, false, CircularConditionalPrior(),
            DNest4::PriorType::log_uniform)
{

}

double BlobbySource::evaluate(double x, double y) const
{
    double f = 0.0;

    const std::vector<std::vector<double>>& components = blobs.get_components();

    double rsq, widthsq, one_over;
    double c = 2.0/M_PI;

    for(size_t i=0; i<components.size(); ++i)
    {
        rsq = pow(x - components[i][0], 2) + pow(y - components[i][1], 2);
        widthsq = components[i][3]*components[i][3];
        one_over = 1.0/widthsq;

        if(rsq < widthsq)
            f += components[i][2]*c*(1.0 - rsq*one_over)*one_over;
    }

    return f;
}

void BlobbySource::from_prior(DNest4::RNG& rng)
{
    blobs.from_prior(rng);
}

double BlobbySource::perturb(DNest4::RNG& rng)
{
    double logH = 0.;

    logH += blobs.perturb(rng);

    return logH;
}

void BlobbySource::print(std::ostream& out) const
{
    blobs.print(out);
}

void BlobbySource::read(std::istream& in)
{
    blobs.read(in);
}

} // namespace Lensing3

