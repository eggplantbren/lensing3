#include "PSF.h"
#include "DNest4/code/Utils.h"

#include <iostream>
#include <fstream>
#include <exception>

namespace Lensing3
{

PSF::PSF(size_t ni, size_t nj)
:ni(ni)
,nj(nj)
,pixels(ni, nj, arma::fill::zeros)
,fft_ready(false)
{
    pixels(ni/2, nj/2) = 1.0;
}

void PSF::load(const char* filename)
{
    std::fstream fin(filename, std::ios::in);
    if(!fin)
    {
        std::cerr<<"# ERROR: couldn't open file "<<filename<<"."<<std::endl;
        return;
    }

    // Storage on disk is in row-major order
    for(size_t i=0; i<ni; ++i)
        for(size_t j=0; j<nj; ++j)
            fin>>pixels(i, j);

    fin.close();
    normalise();
}

void PSF::normalise()
{
    double sum = 0.0;
    for(size_t j=0; j<nj; ++j)
        for(size_t i=0; i<ni; ++i)
            sum += pixels(i, j);

    for(size_t j=0; j<nj; ++j)
        for(size_t i=0; i<ni; ++i)
            pixels(i, j) /= sum;
}

std::ostream& operator << (std::ostream& out, const PSF& psf)
{
    // Storage on disk is in row-major order
    for(size_t i=0; i<psf.ni; ++i)
    {
        for(size_t j=0; j<psf.nj; ++j)
            out<<psf.pixels(i, j)<<' ';
        out<<std::endl;
    }

    return out;
}

void PSF::calculate_fft(size_t ni_image, size_t nj_image, double psf_power)
{
    // Make the psf the same size as the image
    arma::mat psf(ni_image, nj_image, arma::fill::zeros);

    int m, n, sign;
    for(int j=0; j<static_cast<int>(nj); ++j)
    {
        n = DNest4::mod(j - nj/2, static_cast<int>(nj_image));

        for(int i=0; i<static_cast<int>(ni); ++i)
        {
            m = DNest4::mod(i - ni/2, static_cast<int>(ni_image));

            // Remember sign
            sign = 1;
            if(pixels(i, j) < 0.0)
                sign = -1;

            psf(m, n) = sign*pow(std::abs(pixels(i, j)), psf_power);
        }
    }

    // Normalise back to 1
    double tot = 0.0;
    for(size_t j=0; j<nj_image; ++j)
        for(size_t i=0; i<ni_image; ++i)
            tot += psf(i, j);
    for(size_t j=0; j<nj_image; ++j)
        for(size_t i=0; i<ni_image; ++i)
            psf(i, j) /= tot;

    fft_of_psf = fft2(psf);
    fft_ready = true;
}

void PSF::blur(arma::mat& img) const
{
    if(!fft_ready)
        throw std::logic_error("PSF was not ready.");

    // Do the fft of the image
    arma::cx_mat fft_of_img = fft2(img);

    // Convolution. % is armadillo's elementwise multiplication
    arma::cx_mat result = arma::ifft2(fft_of_psf % fft_of_img);

    // Put back in img
    for(size_t j=0; j<img.n_cols; ++j)
        for(size_t i=0; i<img.n_rows; ++i)
            img(i, j) = result(i, j).real();
}

} // namespace Lensing3


