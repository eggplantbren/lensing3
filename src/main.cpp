#include <iostream>
#include "DNest4/code/DNest4.h"
#include "MyModel.h"
#include "Data.h"

int main(int argc, char** argv)
{
    // Load the data
    Lensing3::Data::get_instance().load("../src/data/0047_metadata.txt",
                                        "../src/data/0047_image.txt",
                                        "../src/data/0047_sigma.txt",
                                        "../src/data/0047_psf.txt");

    // Run DNest
    DNest4::start<Lensing3::MyModel>(argc, argv);

    return 0;
}

