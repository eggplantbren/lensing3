Lensing3
========

A significant refactoring of Lensing2.

(c) 2017 Brendon J. Brewer.

LICENCE
=======

MIT. See the LICENSE file for details.

ACKNOWLEDGEMENTS
================

This work is supported by a Marsden Fast-Start grant
from the Royal Society of New Zealand.


DEPENDENCIES
============

* DNest4 (https://www.github.com/eggplantbren/DNest4)
* Armadillo (http://arma.sourceforge.net/)

